import { useEffect, useState } from 'react';
import { api } from '../api/axios';
import useRefresh from '../hooks/useRefresh';

const User = () => {
  const [users, setUsers] = useState([]);
  const controller = new AbortController();
  const refresh = useRefresh();

  const getUsers = async (isMounted) => {
    try {
      const res = await api.get('/users', {
        signal: controller.signal,
      });
      console.log(res);
      isMounted && setUsers(res.data);
    } catch (error) {
      console.log(error.response);
    }
  };

  useEffect(() => {
    let isMounted = true;
    getUsers(isMounted);

    return () => {
      isMounted = false;
      controller.abort();
    };
  }, []);

  return (
    <article>
      {users.length > 0 ? (
        <ul>
          {users.map((user, index) => (
            <li key={index}>{user.username}</li>
          ))}
        </ul>
      ) : (
        <p>No user to display</p>
      )}
      <button onClick={refresh}>Refresh</button>
    </article>
  );
};

export default User;
