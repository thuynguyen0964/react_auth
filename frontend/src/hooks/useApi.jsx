import useRefresh from './useRefresh';
import { privateApi } from '../api/axios';
import useAuth from './useAuth';
import { useEffect } from 'react';

export default function useApi() {
  const { auth } = useAuth();
  const refresh = useRefresh();

  useEffect(() => {
    const responseInter = privateApi.interceptors.response.use(
      (reponse) => reponse,
      async (error) => {
        const preRequest = error?.config;
        if (error?.reponse?.status === 403 && !preRequest.sent) {
          preRequest.sent = true;
          const newAccessToken = await refresh();
          preRequest.Headers['Authorization'] = `Bearer ${newAccessToken}`;
          return privateApi(preRequest);
        }
      }
    );

    return () => {
      privateApi.interceptors.response.eject(responseInter);
    };
  }, [auth, refresh]);

  return privateApi;
}
