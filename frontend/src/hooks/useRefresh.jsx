import { api } from '../api/axios';
import useAuth from './useAuth';

export default function useRefresh() {
  const { setAuth } = useAuth();

  const refresh = async () => {
    const res = await api.get('/refresh');

    setAuth((prev) => {
      console.log(prev);
      const accessToken = res.data?.accessToken;

      console.log('🚀 ~ setAuth ~ accessToken:', accessToken);
      return { ...prev, accessToken };
    });

    return res.data?.accessToken;
  };

  return refresh;
}
