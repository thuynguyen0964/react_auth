import axios from 'axios';
const BASE_URL = 'http://localhost:3500';
const config = { 'Content-Type': 'application/json' };

const api = axios.create({
  baseURL: BASE_URL,
  headers: config,
});

const privateApi = axios.create({
  baseURL: BASE_URL,
  headers: config,
});

api.defaults.withCredentials = true;

export { api, privateApi };
